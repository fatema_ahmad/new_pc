# Printing Calculator
Printing calculator is a system for calculating printing cost and placing order.

# System Analysis

* Customer can select particular category from categories.
* The second page contains the types of the selected category.
* Then the sub categories are shown for a particular type.
* From these sub categories, select a sub category for cost calculation.
* Then customer can add the item to cart.

## Flowchart
![customer](/uploads/2bb052aa5e41fe1a4a624c1cf55f2b3f/customer.png)

## ER-Diagram
![ER-Diagram](/uploads/a5dbd99a0e3faa33adb142d941a6cc01/ER-Diagram.png)
